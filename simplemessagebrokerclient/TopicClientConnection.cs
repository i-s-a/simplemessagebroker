﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using SimpleMessageBrokerClient.Requests;
using SimpleMessageBrokerClient.Responses;

namespace SimpleMessageBrokerClient
{
    public class TopicClientConnection
    {
        private readonly string TOPIC_URL = "topics";
        private readonly string SUBSCRIBER_URL = "subscribers";
        private readonly string MESSAGE_URL = "messages";

        private HttpClient mHttpClient = null;

        public TopicClientConnection(string baseUri)
        {
            Uri uri = new Uri(baseUri);

            mHttpClient = new HttpClient
            {
                BaseAddress = uri
            };

            mHttpClient.DefaultRequestHeaders.Accept.Clear();
            mHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<HttpResponseMessage> PostAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
                await mHttpClient.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> PutAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
            await mHttpClient.PutAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        private async Task<Response> DoPost<Request, Response>(string url, Request request)
        {
            var response = await PostAsJson<Request>(url, request);
            var readResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<Response>(readResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(url,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            }
        }

        public async Task<string> CreateTopic(NewTopicRequest request)
        {
            return await DoPost<NewTopicRequest, string>(TOPIC_URL, request);

            //var response = await PostAsJson<NewTopicRequest>(CREATE_TOPIC_URL, request);
            //var readResponse = await response.Content.ReadAsStringAsync();

            ////var location = response.Headers.Location;

            //if (response.IsSuccessStatusCode)
            //{
            //    var deserializedResponse = JsonConvert.DeserializeObject<string>(readResponse);
            //    return deserializedResponse;
            //    //return location.ToString();
            //}
            //else
            //{
            //    throw new TopicClientException(CREATE_TOPIC_URL,
            //        string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            //}
        }

        public async Task<bool> CreateTopic(string topicId, NewTopicRequest request)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}");
            UpdateTopicRequest updateTopicRequest = new UpdateTopicRequest()
            {
                Name = request.Name,
                NumRetries = request.NumRetries
            };

            var response = await PutAsJson<UpdateTopicRequest>(uri, updateTopicRequest);
            var readResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                //var deserializedResponse = JsonConvert.DeserializeObject<TopicResponse>(readResponse);
                //return deserializedResponse;
                return true;
            }
            else
            {
                throw new TopicClientException(TOPIC_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            }
        }

        public async Task<bool> DeleteTopic(string topicId)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}");
            var response = await mHttpClient.DeleteAsync(uri);
            var deleteResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                throw new TopicClientException(TOPIC_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), deleteResponse));
            }
        }

        public async Task<string> UpdateTopic(string topicId, UpdateTopicRequest request)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}");
            var response = await PutAsJson<UpdateTopicRequest>(uri, request);
            var readResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<string>(readResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(TOPIC_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            }
        }

        public async Task<TopicResponse> GetTopic(string topicId)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}");
            return await GetTopicByUrl(uri);
        }

        public async Task<TopicResponse> GetTopicByUrl(string url)
        {
            var response = await mHttpClient.GetAsync(url);
            var topicResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<TopicResponse>(topicResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(TOPIC_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), topicResponse));
            }
        }

        public async Task<List<TopicResponse>> GetAllTopics()
        {
            var response = await mHttpClient.GetAsync(TOPIC_URL);
            var topicsResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<List<TopicResponse>>(topicsResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(TOPIC_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), topicsResponse));
            }
        }

        public async Task<string> Subscribe(string topicId, NewTopicSubscriberRequest request)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}/{SUBSCRIBER_URL}");
            return await DoPost<NewTopicSubscriberRequest, string>(uri, request);
        }

        public async Task UpdateSubscriber(string topicId, string subscriberId, UpdateTopicSubscriberRequest request)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}/{SUBSCRIBER_URL}/{subscriberId}");
            var response = await PutAsJson<UpdateTopicSubscriberRequest>(uri, request);
            var subscriberResponse = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new TopicClientException(SUBSCRIBER_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), subscriberResponse));
            }
        }

        public async Task Unsubscribe(string topicId, string subscriberId)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}/{SUBSCRIBER_URL}/{subscriberId}");
            var response = await mHttpClient.DeleteAsync(uri);
            var deleteResponse = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new TopicClientException(SUBSCRIBER_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), deleteResponse));
            }
        }

        public async Task<List<TopicSubscriberResponse>> GetAllTopicSubscribers(string topicId)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}/{SUBSCRIBER_URL}");

            var response = await mHttpClient.GetAsync(uri);
            var subscribersResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<List<TopicSubscriberResponse>>(subscribersResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(SUBSCRIBER_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), subscribersResponse));
            }
        }

        public async Task<TopicSubscriberResponse> GetTopicSubscriber(string topicId, string subscriberId)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}/{SUBSCRIBER_URL}/{subscriberId}");

            var response = await mHttpClient.GetAsync(uri);
            var subscribersResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<TopicSubscriberResponse>(subscribersResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(SUBSCRIBER_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), subscribersResponse));
            }
        }

        public async Task PublishMessageToTopic<T>(string topicId, T request)
        {
            string uri = string.Format($"{TOPIC_URL}/{topicId}/{MESSAGE_URL}");
            var response = await PostAsJson<T>(uri, request);
            var publishResponse = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new TopicClientException(uri,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), publishResponse));
            }
        }
    }
}
