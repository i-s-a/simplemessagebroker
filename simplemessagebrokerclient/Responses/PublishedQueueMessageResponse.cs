﻿using Newtonsoft.Json;

namespace SimpleMessageBrokerClient.Responses
{
    [JsonObject("message")]
    public class PublishedQueueMessageResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("ref")]
        public string Ref { get; set; }
    }
}
