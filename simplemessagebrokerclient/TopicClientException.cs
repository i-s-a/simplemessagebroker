﻿using System;

namespace SimpleMessageBrokerClient
{
    class TopicClientException : Exception
    {
        public TopicClientException(string baseUri, string message)
            : base(message)
        {
        }

        public TopicClientException(string baseUri, string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public string BaseUri { get; set; }
    }
}
