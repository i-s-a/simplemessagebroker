﻿using Newtonsoft.Json;

namespace SimpleMessageBrokerClient.Requests
{
    [JsonObject("new_queue")]
    public class NewQueueRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("num_retries")]
        public int NumRetries { get; set; }
    }
}
