﻿using Newtonsoft.Json;

namespace SimpleMessageBrokerClient.Requests
{
    [JsonObject("publish_message")]
    public class PublishTopicMessageRequest
    {
        [JsonProperty("msg_content")]
        public string MessageContent { get; set; }
    }
}
