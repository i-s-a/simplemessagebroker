﻿using Newtonsoft.Json;

namespace SimpleMessageBrokerClient.Requests
{
    [JsonObject("new_subscriber")]
    public class NewTopicSubscriberRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("notification_url")]
        public string NotificationUrl { get; set; }
    }
}
