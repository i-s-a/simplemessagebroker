﻿using Newtonsoft.Json;

namespace SimpleMessageBrokerClient.Requests
{
    [JsonObject("update_topic")]
    public class UpdateTopicRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("num_retries")]
        public int NumRetries { get; set; }
    }
}
