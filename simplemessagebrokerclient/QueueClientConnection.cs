﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using SimpleMessageBrokerClient.Requests;
using SimpleMessageBrokerClient.Responses;

namespace SimpleMessageBrokerClient
{
    public class QueueClientConnection
    {
        private readonly string QUEUE_URL = "queues";

        private HttpClient mHttpClient = null;

        public QueueClientConnection(string baseUri)
        {
            Uri uri = new Uri(baseUri);

            mHttpClient = new HttpClient
            {
                BaseAddress = uri
            };

            mHttpClient.DefaultRequestHeaders.Accept.Clear();
            mHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<HttpResponseMessage> PostAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
                await mHttpClient.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> PutAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
            await mHttpClient.PutAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        private async Task<Response> DoPost<Request, Response>(string url, Request request)
        {
            var response = await PostAsJson<Request>(url, request);
            var readResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<Response>(readResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(url,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            }
        }

        public async Task<string> CreateQueue(NewQueueRequest request)
        {
            return await DoPost<NewQueueRequest, string>(QUEUE_URL, request);
        }

        public async Task<bool> CreateQueue(string queueId, NewQueueRequest request)
        {
            string uri = string.Format("{0}/{1}", QUEUE_URL, queueId);
            UpdateQueueRequest updateQueueRequest = new UpdateQueueRequest()
            {
                Name = request.Name,
                NumRetries = request.NumRetries
            };

            var response = await PutAsJson<UpdateQueueRequest>(uri, updateQueueRequest);
            var readResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                throw new TopicClientException(QUEUE_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            }
        }

        public async Task<bool> DeleteQueue(string queueId)
        {
            string uri = string.Format($"{QUEUE_URL}/{queueId}");
            var response = await mHttpClient.DeleteAsync(uri);
            var deleteResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                throw new TopicClientException(QUEUE_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), deleteResponse));
            }
        }

        public async Task<string> UpdateQueue(string queueId, UpdateQueueRequest request)
        {
            string uri = string.Format($"{QUEUE_URL}/{queueId}");
            var response = await PutAsJson<UpdateQueueRequest>(uri, request);
            var readResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<string>(readResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(QUEUE_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            }
        }

        public async Task<QueueResponse> GetQueue(string queueId)
        {
            string uri = string.Format($"{QUEUE_URL}/{queueId}");
            return await GetQueueByUrl(uri);
        }

        public async Task<QueueResponse> GetQueueByUrl(string url)
        {
            var response = await mHttpClient.GetAsync(url);
            var queueResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<QueueResponse>(queueResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(QUEUE_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), queueResponse));
            }
        }

        public async Task<List<QueueResponse>> GetAllQueues()
        {
            var response = await mHttpClient.GetAsync(QUEUE_URL);
            var queuesResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<List<QueueResponse>>(queuesResponse);
                return deserializedResponse;
            }
            else
            {
                throw new TopicClientException(QUEUE_URL,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), queuesResponse));
            }
        }
    }
}
