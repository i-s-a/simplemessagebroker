﻿using System;
using System.Threading.Tasks;
using SimpleMessageBrokerClient;
using SimpleMessageBrokerClient.Requests;
using SimpleMessageBrokerClient.Responses;
using SimpleMessageBrokerClientTest.PublishMessages;
using Newtonsoft.Json;

namespace SimpleMessageBrokerClientTest
{
    class Program
    {
        private static string simpleMesageBrokerUri = "http://localhost:5000/api/v1/";
        //private static string simpleMesageBrokerUri = "http://localhost:51065/api/v1/";

        private static async Task RunTopicTests()
        {
            try
            {
                var topicClient = new TopicClientConnection(simpleMesageBrokerUri);

                var topicResponseId = await topicClient.CreateTopic(new NewTopicRequest()
                {
                    Name = "Topic_1",
                    NumRetries = 0
                });

                Console.WriteLine("Topic:  {0} created", topicResponseId);

                var topicResponse1 = await topicClient.GetTopic(topicResponseId);
                Console.WriteLine("Retrieved: {0} - {1}", topicResponse1.Id, topicResponse1.Name);


                var topicResponseId2 = await topicClient.CreateTopic("My_Topic_1", new NewTopicRequest()
                {
                    Name = "Topic_2",
                    NumRetries = 1
                });

                var topicResponse2 = await topicClient.GetTopic("My_Topic_1");
                Console.WriteLine("Retrieved: {0} - {1}", topicResponse2.Id, topicResponse2.Name);
                Console.WriteLine();
                Console.WriteLine();

                var allTopicsResponse = await topicClient.GetAllTopics();
                foreach (var topic in allTopicsResponse)
                {
                    Console.WriteLine("{0} - {1}", topic.Id, topic.Name);
                }

                Console.WriteLine();
                Console.WriteLine();

                var deleteResponse = await topicClient.DeleteTopic(topicResponseId);
                Console.WriteLine("Deleted: {0}", topicResponseId);
                Console.WriteLine();

                allTopicsResponse = await topicClient.GetAllTopics();
                foreach (var topic in allTopicsResponse)
                {
                    Console.WriteLine("{0} - {1}", topic.Id, topic.Name);
                }

                Console.WriteLine();
                Console.WriteLine();

                await topicClient.UpdateTopic(topicResponse2.Id, new UpdateTopicRequest()
                {
                    Name = "Topic_3",
                    NumRetries = 2
                });
                Console.WriteLine("Updated {0} to Topic_3", topicResponse2.Id);

                allTopicsResponse = await topicClient.GetAllTopics();
                foreach (var topic in allTopicsResponse)
                {
                    Console.WriteLine("{0} - {1}", topic.Id, topic.Name);
                }

                var singleTopicResponse = await topicClient.GetTopic(topicResponse2.Id);
                Console.WriteLine("{0} - {1}", singleTopicResponse.Id, singleTopicResponse.Name);

                Console.WriteLine();
                Console.WriteLine();

                var subscriber1 = await topicClient.Subscribe(singleTopicResponse.Id, new NewTopicSubscriberRequest()
                {
                    Name = "Joe Bloggs",
                    NotificationUrl = "http://localhost"
                });

                Console.WriteLine("Subscriber: {0}", subscriber1);

                var retrievedSubscriber1 = await topicClient.GetTopicSubscriber(singleTopicResponse.Id, subscriber1);
                Console.WriteLine("Retrieved subscriber: {0}, {1}, {2}", retrievedSubscriber1.Id, retrievedSubscriber1.Name, retrievedSubscriber1.NotificationUrl);

                var subscriber2 = await topicClient.Subscribe(singleTopicResponse.Id, new NewTopicSubscriberRequest()
                {
                    Name = "Jane Doe",
                    NotificationUrl = "http://localhost"
                });

                await topicClient.UpdateSubscriber(singleTopicResponse.Id, subscriber2, new UpdateTopicSubscriberRequest()
                {
                    NewName = "Jane Doe",
                    NotificationUrl = "http://111.111.111.111",
                });

                var retrievedSubscriber2 = await topicClient.GetTopicSubscriber(singleTopicResponse.Id, subscriber2);
                Console.WriteLine("Retrieved subscriber: {0}, {1}, {2}", retrievedSubscriber2.Id, retrievedSubscriber2.Name, retrievedSubscriber2.NotificationUrl);

                Console.WriteLine();
                Console.WriteLine();

                var topicSubscribers = await topicClient.GetAllTopicSubscribers(singleTopicResponse.Id);

                foreach (var topicSubscriber in topicSubscribers)
                {
                    Console.WriteLine("Retrieved: {0}, {1}, {2}", topicSubscriber.Id, topicSubscriber.Name, topicSubscriber.NotificationUrl);
                }

                await topicClient.Unsubscribe(singleTopicResponse.Id, subscriber1);

                Console.WriteLine();
                Console.WriteLine();

                topicSubscribers = await topicClient.GetAllTopicSubscribers(singleTopicResponse.Id);

                foreach (var topicSubscriber in topicSubscribers)
                {
                    Console.WriteLine("Retrieved: {0}, {1}, {2}", topicSubscriber.Id, topicSubscriber.Name, topicSubscriber.NotificationUrl);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}", ex.Message);
            }
        }

        private static async Task RunPublishTests()
        {
            try
            {
                var topicClient = new TopicClientConnection(simpleMesageBrokerUri);

                var topicId = await topicClient.CreateTopic(new NewTopicRequest()
                {
                    Name = "topic1",
                    NumRetries = 0
                });

                Console.WriteLine("Topic:  {0} created", topicId);

                var subscriber1 = await topicClient.Subscribe(topicId, new NewTopicSubscriberRequest()
                {
                    Name = "Joe Bloggs",
                    NotificationUrl = "http://localhost:[port here]/users/topic1messages"
                });

                Console.WriteLine("Subscriber:  {0} subscribed to topic {1}", subscriber1, topicId);

                AssociateAnimalWithOwner associateAnimalWithOwner = new AssociateAnimalWithOwner()
                {
                    UserId = "123",
                    AnimalId = "456"
                };

                string serializedMsg = JsonConvert.SerializeObject(associateAnimalWithOwner);

                PublishTopicMessageRequest publishTopicMessageRequest = new PublishTopicMessageRequest()
                {
                    MessageContent = serializedMsg
                };

                await topicClient.PublishMessageToTopic<PublishTopicMessageRequest>(topicId, publishTopicMessageRequest);

            }
            catch (Exception ex)
            {
                System.Console.WriteLine("{0}", ex.Message);
            }
        }

        private static async Task RunQueueTests()
        {
            try
            {
                var queueClient = new QueueClientConnection(simpleMesageBrokerUri);

                var queueId1 = await queueClient.CreateQueue("My_Queue_1", new NewQueueRequest()
                {
                    Name = "Queue1",
                    NumRetries = 0
                });

                Console.WriteLine("Queue:  My_Queue_1 created/updated");

                var queueId2 = await queueClient.CreateQueue(new NewQueueRequest()
                {
                    Name = "Queue2",
                    NumRetries = 0
                });

                Console.WriteLine("Queue: {0} created", queueId2);

                var retrievedQueue1 = await queueClient.GetQueue("My_Queue_1");

                Console.WriteLine("Retrieved queue {0}", retrievedQueue1.Id);

                var retrievedQueue2 = await queueClient.GetQueue(queueId2);

                Console.WriteLine("Retrieved queue {0}", retrievedQueue2.Id);

                var updatedQueueId2 = await queueClient.UpdateQueue(queueId2, new UpdateQueueRequest()
                {
                    Name = "Queue2_Updated",
                    NumRetries = 0
                });

                Console.WriteLine();

                var allQueues = await queueClient.GetAllQueues();
                foreach (var queue in allQueues)
                {
                    Console.WriteLine("Retrieved updated queue {0}", queue.Name);
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("{0}", ex.Message);
            }
        }

        static async Task Main(string[] args)
        {
            await RunTopicTests();
            //await RunPublishTests();
            //await RunQueueTests();
            System.Console.ReadKey();
        }
    }
}
