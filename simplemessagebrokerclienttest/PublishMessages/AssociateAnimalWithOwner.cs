﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace SimpleMessageBrokerClientTest.PublishMessages
{
    [JsonObject("associate_animal_with_owner")]
    public class AssociateAnimalWithOwner
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("animal_id")]
        public string AnimalId { get; set; }
    }
}
