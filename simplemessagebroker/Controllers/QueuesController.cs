﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleMessageBroker.Services;
using SimpleMessageBroker.Requests;

namespace SimpleMessageBroker.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class QueuesController : ControllerBase
    {
        private readonly QueueService mQueueService = null;

        public QueuesController(QueueService queueService)
        {
            mQueueService = queueService;
        }

        // GET api/queues
        [HttpGet]
        public async Task<IActionResult> GetAllQueues(int? offset, int? count)
        {
            try
            {
                var response = await mQueueService.GetAllQueues(offset, count);

                return Ok(response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/queues/5
        [HttpGet("{queueId}")]
        public async Task<IActionResult> GetQueue(string queueId)
        {
            try
            {
                var response = await mQueueService.GetQueue(queueId);

                return Ok(response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/queues
        [HttpPost]
        public async Task<IActionResult> CreateQueue([FromBody]NewQueueRequest request)
        {
            try
            {
                var response = await mQueueService.AddQueue(request);

                return Created("/queues/" + response, response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT api/queues/5
        [HttpPut("{queueId}")]
        public async Task<IActionResult> UpdateQueue(string queueId, [FromBody]UpdateQueueRequest request)
        {
            try
            {
                var response = await mQueueService.UpdateQueue(queueId, request);

                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE api/queues/5
        [HttpDelete("{queueId}")]
        public async Task<IActionResult> DeleteQueue(string queueId)
        {
            try
            {
                await mQueueService.DeleteQueue(queueId);

                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/queues/5/messages
        [HttpPost("{queueId}/messages")]
        public async Task<IActionResult> PublishMessageToQueue(string queueId, [FromBody]PublishQueueMessageRequest request)
        {
            try
            {
                var response = await mQueueService.PublishMessageToQueue(queueId, request);

                return Created("/queues/" + queueId + "/messages/" + response.ToString(), response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/queues/5/messages?offset=xx&count=xx
        [HttpGet("{queueId}/messages")]
        public async Task<IActionResult> GetAllMessages(string queueId, int? offset, int? count)
        {
            try
            {
                var response = await mQueueService.GetQueueMessages(queueId, offset, count);

                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/queues/5/messages/2
        [HttpGet("{queueId}/messages/{messageId}")]
        public async Task<IActionResult> GetMessage(string queueId, int messageId)
        {
            try
            {
                var response = await mQueueService.GetQueueMessage(queueId, messageId);

                return Ok(response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
