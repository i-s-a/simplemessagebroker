﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleMessageBroker.Services;
using SimpleMessageBroker.Requests;

namespace SimpleMessageBroker.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TopicsController : ControllerBase
    {
        private readonly TopicService mTopicService = null;

        public TopicsController(TopicService topicService)
        {
            mTopicService = topicService;
        }

        // GET api/topics
        [HttpGet]
        public async Task<IActionResult> GetAllTopics()
        {
            try
            {
                var response = await mTopicService.GetAllTopics();

                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/topics/5
        [HttpGet("{topicId}")]
        public async Task<IActionResult> GetTopic(string topicId)
        {
            try
            {
                var response = await mTopicService.GetTopic(topicId);

                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/topics
        [HttpPost]
        public async Task<IActionResult> CreateTopic([FromBody]NewTopicRequest request)
        {
            try
            {
                var response = await mTopicService.AddTopic(request);

                return Created("/topics/" + response, response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT api/topics/5
        [HttpPut("{topicId}")]
        public async Task<IActionResult> UpdateTopic(string topicId, [FromBody]UpdateTopicRequest request)
        {
            try
            {
                await mTopicService.UpdateTopic(topicId, request);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE api/topics/5
        [HttpDelete("{topicId}")]
        public async Task<IActionResult> DeleteTopic(string topicId)
        {
            try
            {
                await mTopicService.DeleteTopic(topicId);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/topics/5/messages
        [HttpPost("{topicId}/messages")]
        public async Task<IActionResult> PublishMessageToTopic(string topicId, PublishTopicMessageRequest request)
        {
            try
            {
                await mTopicService.PublishMessageToTopic(topicId, request);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Perhaps break out the subscriber endpoints to a separate SubscribersController!!!!!

        // POST api/topics/6/subscribers/
        [HttpPost("{topicId}/subscribers")]
        public async Task<IActionResult> Subscribe(string topicId, [FromBody]NewTopicSubscriberRequest request)
        {
            try
            {
                var response = await mTopicService.AddSubscriber(topicId, request);

                return Created("/topics/" + topicId + "/subscribers/" + response + "/", response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT api/topics/6/subscribers/3
        [HttpPut("{topicId}/subscribers/{subscriberId}")]
        public async Task<IActionResult> UpdateSubscriber(string topicId, string subscriberId, [FromBody]UpdateTopicSubscriberRequest request)
        {
            try
            {
                await mTopicService.UpdateSubscriber(topicId, subscriberId, request);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PATCH api/topics/6/subscribers/3
        [HttpPatch("{topicId}/subscribers/{subscriberId}")]
        public IActionResult PartialUpdateSubscriber(string topicId, string subscriberId, [FromBody]UpdateTopicSubscriberRequest request)
        {
            try
            {
                return StatusCode(500, "Not yet implemented");
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE api/topics/6/subscribers/3
        [HttpDelete("{topicId}/subscribers/{subscriberId}")]
        public async Task<IActionResult> Unsubscribe(string topicId, string subscriberId)
        {
            try
            {
                await mTopicService.DeleteSubscriber(topicId, subscriberId);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/topics/6/subscribers
        [HttpGet("{topicId}/subscribers")]
        public async Task<IActionResult> GetAllSubscribers(string topicId)
        {
            try
            {
                var response = await mTopicService.GetSubsribers(topicId);

                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/topics/6/subscribers
        [HttpGet("{topicId}/subscribers/{subscriberId}")]
        public async Task<IActionResult> GetSubscriber(string topicId, string subscriberId)
        {
            try
            {
                var response = await mTopicService.GetSubscriber(topicId, subscriberId);

                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
