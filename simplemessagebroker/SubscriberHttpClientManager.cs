﻿using System.Collections.Generic;
using System.Threading;

namespace SimpleMessageBroker
{
    public class SubscriberHttpClientManager
    {
        private Dictionary<string, SubscriberHttpClient> mSubscriberClients = new Dictionary<string, SubscriberHttpClient>();
        private ReaderWriterLockSlim mRwLock = new ReaderWriterLockSlim();

        public SubscriberHttpClientManager()
        {
        }

        public void Add(string uri, SubscriberHttpClient subscriberHttpClient)
        {
            mRwLock.EnterWriteLock();
            try
            {
                mSubscriberClients[uri] = subscriberHttpClient;
            }
            finally
            {
                mRwLock.ExitWriteLock();
            }
        }

        public void Delete(string uri)
        {
            mRwLock.EnterWriteLock();
            try
            {
                mSubscriberClients.Remove(uri);
            }
            finally
            {
                mRwLock.ExitWriteLock();
            }
        }

        public SubscriberHttpClient Get(string uri)
        {
            mRwLock.EnterReadLock();

            SubscriberHttpClient subscriberHttpClient = null;

            try
            {
                mSubscriberClients.TryGetValue(uri, out subscriberHttpClient);
            }
            finally
            {
                mRwLock.ExitReadLock();
            }

            return subscriberHttpClient;
        }

        public bool Exists(string uri)
        {
            mRwLock.EnterReadLock();

            try
            {
                return mSubscriberClients.ContainsKey(uri);
            }
            finally
            {
                mRwLock.ExitReadLock();
            }
        }
    }
}
