﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using SimpleMessageBroker.Requests;
using SimpleMessageBroker.Models;
using SimpleMessageBroker.Responses;
using SimpleMessageBroker.Repositories;

namespace SimpleMessageBroker.Services
{
    public class TopicService
    {
        private readonly ITopicRepository mRepository = null;
        private readonly SubscriberHttpClientManager mSubscriberHttpClientManager = null;

        public TopicService(ITopicRepository repository, SubscriberHttpClientManager subscriberHttpClientManager)
        {
            mRepository = repository;
            mSubscriberHttpClientManager = subscriberHttpClientManager;
        }

        public async Task<string> AddTopic(NewTopicRequest request)
        {
            //We don't allow queues with empty names
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Invalid topic name");

            string topicId = Guid.NewGuid().ToString();
            var topic = new Topic()
            {
                Id = topicId,
                Name = request.Name,
                Subscribers = new List<Subscriber>()
            };

            await mRepository.AddAsync(topic);

            return topicId;
        }

        public async Task DeleteTopic(string topicId)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");

            await mRepository.DeleteAsync(topic);
        }

        public async Task<string> UpdateTopic(string topicId, UpdateTopicRequest request)
        {
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Invalid topic name");

            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
            {
                //Topic doesn't exist, therefore create a new one
                topic = new Topic()
                {
                    Id = topicId,       //For now assume the ID and the name will be the same
                    Name = request.Name,
                    Subscribers = new List<Subscriber>()
                };

                await mRepository.AddAsync(topic);
            }
            else
            {
                //Topic already exists so update the name
                topic.Name = request.Name;

                await mRepository.UpdateAsync(topic);
            }

            return topic.Id;
        }

        public async Task<TopicResponse> GetTopic(string topicId)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");

            var topicResponse = GetTopicResponse(topic);

            return topicResponse;
        }

        public async Task<List<TopicResponse>> GetAllTopics()
        {
            var topics = await mRepository.GetAllAsync();

            if (topics == null)
                throw new NotFoundException("No topics were found");

            var topicResponseList = GetTopicResponseList(topics);

            return topicResponseList;
        }


        //Perhaps break the subscriber methods in to their own SubscriberService!!!!!

        public async Task<string> AddSubscriber(string topicId, NewTopicSubscriberRequest request)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");

            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Invalid subscriber name");

            if (!IsValidUrl(request.NotificationUrl))
                throw new ArgumentException("Invalid notification url");

            string subscriberId = Guid.NewGuid().ToString();
            var subscriber = new Subscriber()
            {
                Id = subscriberId,
                Name = request.Name,
                NotificationUrl = request.NotificationUrl
            };

            topic.Subscribers.Add(subscriber);

            await mRepository.UpdateAsync(topic);

            return subscriberId;
        }

        public async Task UpdateSubscriber(string topicId, string subscriberId, UpdateTopicSubscriberRequest request)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");

            if (string.IsNullOrEmpty(request.NewName))
                throw new ArgumentException("Invalid subscriber name");

            if (!IsValidUrl(request.NotificationUrl))
                throw new ArgumentException("Invalid notification url");

            var subscriber = topic.Subscribers.SingleOrDefault(x => x.Id == subscriberId);

            if (subscriber == null)
                throw new NotFoundException("Subscriber not found");

            subscriber.Name = request.NewName;
            subscriber.NotificationUrl = request.NotificationUrl;

            await mRepository.UpdateAsync(topic);
        }

        public async Task DeleteSubscriber(string topicId, string subscriberId)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");

            var subscriber = topic.Subscribers.SingleOrDefault(x => x.Id == subscriberId);

            if (subscriber == null)
                throw new NotFoundException("Subscriber not found");

            topic.Subscribers.Remove(subscriber);

            await mRepository.UpdateAsync(topic);
        }

        public async Task<TopicSubscriberResponse> GetSubscriber(string topicId, string subscriberId)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");

            var subscriber = topic.Subscribers.SingleOrDefault(x => x.Id == subscriberId);

            if (subscriber == null)
                throw new NotFoundException("Subscriber not found");

            var subscriberResponse = GetSubscriberResponse(subscriber);

            return subscriberResponse;
        }

        public async Task<List<TopicSubscriberResponse>> GetSubsribers(string topicId)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");

            var subscriberResponseList = GetSubscriberResponseList(topic);

            return subscriberResponseList;
        }

        public async Task PublishMessageToTopic(string topicId, PublishTopicMessageRequest request)
        {
            var topic = await mRepository.GetAsync(topicId);

            if (topic == null)
                throw new NotFoundException("Topic not found");


            var tasks = new List<Task<bool>>();
            var subscribersToRemove = new List<Subscriber>();

            foreach (var subscriber in topic.Subscribers)
            {
                //According to Microsoft documentation the same HttpClient can be used safely from multiple
                //threads. This means we can reuse the same HtppClient to send messages to the same base URI
                //even if the request URIs are different.
                //E.g.  www.example.com/messages
                //      www.example.com/messages/1
                //      www.example.com/subscribers
                //All the above can be called from the same HttpClient
                //Therefore we can store a single HttpClient against a base uri and any messages that need to
                //be published to the same base URI can use the same HttpClient.

                Uri uri = new Uri(subscriber.NotificationUrl);
                string baseUri = uri.GetLeftPart(UriPartial.Authority);
                string requestUri = uri.PathAndQuery;

                //Do we already have a client for this base URI? If so then we can just use that to publish
                //the message
                SubscriberHttpClient subscriberHttpClient = mSubscriberHttpClientManager.Get(baseUri);

                if (subscriberHttpClient == null)
                {
                    //We don't have a client for this base URI so add it to the manager now.

                    //It is possible that more than one call could get here at the same time adding a client
                    //with the same URI. This would only happen if more than one request to publish a message
                    //is received for a base URI that hasn't been seen before and is therefore not yet in the
                    //http client manager. This shouldn't be a problem because the Add method will simply update
                    //the base URI with the last client added, and that client will be the one used to publish
                    //messages to the base URI in future requests
                    subscriberHttpClient = new SubscriberHttpClient(baseUri);
                    mSubscriberHttpClientManager.Add(baseUri, subscriberHttpClient);
                }

                var task = subscriberHttpClient.PostAsJson(subscriber.Id, requestUri, new MessageToSubscriber()
                {
                    TimeStamp = DateTime.Now,
                    Content = request.MessageContent
                });

                tasks.Add(task);
            }

            Task taskToAwait = Task.WhenAll(tasks);
            try
            {
                await taskToAwait;
            }
            catch (SubscriberSendException /*ex*/)
            {
                //Something went wrong with this subscriber. For now, if an error occurs
                //sending to a subscriber, remove the subscriber from the topic. Later
                //when logging is added, log that something went wrong sending the message
                //and keep track of the number of failures to the subscriber. If the number
                //of failures reaches a configurable threshold, only then remove the 
                //subscriber from the list

                foreach (var exception in taskToAwait.Exception.InnerExceptions)
                {
                    SubscriberSendException e = (SubscriberSendException)exception;

                    //Log the exception here!!!

                    await DeleteSubscriber(topicId, e.Id);
                }
            }
            catch (Exception /*ex*/)
            {
                
            }

        }

        private bool IsValidUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return false;

            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                return false;

            Uri uri = new Uri(url);
            if (uri.Scheme != Uri.UriSchemeHttp && uri.Scheme != Uri.UriSchemeHttps)
                return false;

            //TODO
            //Check that we can reach the url as well

            return true;
        }

        private List<TopicResponse> GetTopicResponseList(List<Topic> simpleTopics)
        {
            List<TopicResponse> topicResponseList = new List<TopicResponse>();

            foreach (var topic in simpleTopics)
            {
                var topicResponse = GetTopicResponse(topic);

                topicResponseList.Add(topicResponse);
            }

            return topicResponseList;
        }

        private TopicResponse GetTopicResponse(Topic topic)
        {
            var subscriberResponseList = GetSubscriberResponseList(topic);

            var topicResponse = new TopicResponse()
            {
                Id = topic.Id,
                Name = topic.Name,
                Subscribers = subscriberResponseList,
                Ref = ""    //Leave empty for now!!
            };

            return topicResponse;
        }

        private List<TopicSubscriberResponse> GetSubscriberResponseList(Topic topic)
        {
            List<TopicSubscriberResponse> subscriberResponseList = GetSubscriberResponseList(topic.Subscribers);
            return subscriberResponseList;
        }

        private List<TopicSubscriberResponse> GetSubscriberResponseList(List<Subscriber> subscribers)
        {
            List<TopicSubscriberResponse> subscriberResponseList = new List<TopicSubscriberResponse>(subscribers.Count);

            foreach (var subscriber in subscribers)
            {
                var subscriberResponse = GetSubscriberResponse(subscriber);

                subscriberResponseList.Add(subscriberResponse);
            }

            return subscriberResponseList;
        }

        private TopicSubscriberResponse GetSubscriberResponse(Subscriber subscriber)
        {
            return new TopicSubscriberResponse()
            {
                Id = subscriber.Id,
                Name = subscriber.Name,
                NotificationUrl = subscriber.NotificationUrl,
            };
        }
    }
}
