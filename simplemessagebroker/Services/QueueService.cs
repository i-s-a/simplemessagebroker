﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using SimpleMessageBroker.Requests;
using SimpleMessageBroker.Models;
using SimpleMessageBroker.Responses;
using SimpleMessageBroker.Repositories;

namespace SimpleMessageBroker.Services
{
    public class QueueService
    {
        private readonly IQueueRepository mRepository = null;

        public QueueService(IQueueRepository repository)
        {
            mRepository = repository;
        }

        public async Task<string> AddQueue(NewQueueRequest request)
        {
            //We don't allow queues with empty names
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Invalid queue name");

            string queueId = Guid.NewGuid().ToString();
            var queue = new Queue()
            {
                Id = queueId,
                Name = request.Name,
                Messages = new List<PublishedQueueMessage>()
            };

            await mRepository.AddAsync(queue);

            return queueId;
        }

        public async Task DeleteQueue(string queueId)
        {
            var queue = await mRepository.GetAsync(queueId);

            if (queue == null)
                throw new NotFoundException("Queue not found");

            await mRepository.DeleteAsync(queue);
        }

        public async Task<string> UpdateQueue(string queueId, UpdateQueueRequest request)
        {
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Invalid queue name");

            var queue = await mRepository.GetAsync(queueId);

            if (queue == null)
            {
                //Queue doesn't exist, therefore create a new one
                queue = new Queue()
                {
                    Id = queueId,
                    Name = request.Name,
                    Messages = new List<PublishedQueueMessage>()
                };

                await mRepository.AddAsync(queue);
            }
            else
            {
                //Queue already exists so update the name
                queue.Name = request.Name;

                await mRepository.UpdateAsync(queue);
            }

            return queue.Id;
        }

        public async Task<QueueResponse> GetQueue(string queueId)
        {
            var queue = await mRepository.GetAsync(queueId);

            if (queue == null)
                throw new NotFoundException("Queue not found");

            var queueResponse = GetQueueResponse(queue);

            return queueResponse;
        }

        public async Task<List<QueueResponse>> GetAllQueues(int? offset, int? count)
        {
            List<Queue> queues = null;

            if (offset.HasValue && count.HasValue)
                queues = await mRepository.GetAllAsync(offset.Value, count.Value);
            else
                queues = await mRepository.GetAllAsync();

            if (queues == null)
                throw new NotFoundException("No queues were found");

            var queueResponseList = GetQueueResponseList(queues);

            return queueResponseList;
        }

        public async Task<int> PublishMessageToQueue(string queueId, PublishQueueMessageRequest request)
        {
            var queue = await mRepository.GetAsync(queueId);

            if (queue == null)
                throw new NotFoundException("Queue not found");

            DateTime now = DateTime.Now;

            PublishedQueueMessage publishedQueueMessage = new PublishedQueueMessage()
            {
                QueueId = queueId,
                TimeStamp = now,
                JsonMessageContent = request.MessageContent
            };

            queue.Messages.Add(publishedQueueMessage);

            await mRepository.UpdateAsync(queue);

            return publishedQueueMessage.Id;
        }

        public async Task<PublishedQueueMessageResponse> GetQueueMessage(string queueId, int messageId)
        {
            var message = await mRepository.GetQueueMessage(queueId, messageId);

            if (message == null)
                throw new NotFoundException("Not found");

            var queueMessageResponse = GetQueueMessageResponse(message);

            return queueMessageResponse;
        }

        public async Task<List<PublishedQueueMessageResponse>> GetQueueMessages(string queueId, int? offset, int? count)
        {
            if (offset.HasValue && offset.Value < 1)
                throw new ArgumentException("Invalid offset");

            if (count.HasValue && count.Value < 1)
                throw new ArgumentException("Invalid count");

            var messages = await mRepository.GetQueueMessages(queueId, offset, count);

            if (messages == null)
                throw new NotFoundException("Not found");

            var queueMessageResponseList = GetQueueMessageResponseList(messages);

            return queueMessageResponseList;
        }

        private List<QueueResponse> GetQueueResponseList(List<Queue> simpleQueues)
        {
            List<QueueResponse> queueResponseList = new List<QueueResponse>();

            foreach(var queue in simpleQueues)
            {
                var queueResponse = GetQueueResponse(queue);

                queueResponseList.Add(queueResponse);
            }

            return queueResponseList;
        }

        private QueueResponse GetQueueResponse(Queue queue)
        {
            var queueResponse = new QueueResponse()
            {
                Id = queue.Id,
                Name = queue.Name,
                Ref = ""    //Leave empty for now!!
            };

            return queueResponse;
        }

        private PublishedQueueMessageResponse GetQueueMessageResponse(PublishedQueueMessage message)
        {
            return new PublishedQueueMessageResponse()
            {
                Id = message.Id,
                Content = message.JsonMessageContent,
                Ref = ""    //Leave empty for now!!
            };
        }
        private List<PublishedQueueMessageResponse> GetQueueMessageResponseList(List<PublishedQueueMessage> messages)
        {
            List<PublishedQueueMessageResponse> queueMessages = new List<PublishedQueueMessageResponse>(messages.Count);

            foreach (var message in messages)
            {
                var messageResponse = GetQueueMessageResponse(message);
                queueMessages.Add(messageResponse);
            }

            return queueMessages;
        }        
    }
}
