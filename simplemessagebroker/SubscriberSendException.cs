﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleMessageBroker
{
    public class SubscriberSendException : Exception
    {
        public SubscriberSendException(string id, string baseUri, string message) 
            : base(message)
        {
            Id = id;
        }

        public SubscriberSendException(string id, string baseUri, string message, Exception innerException) 
            : base(message, innerException)
        {
            Id = id;
        }

        public string Id { get; set; }
        public string BaseUri { get; set; }
    }
}
