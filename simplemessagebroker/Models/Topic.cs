﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace SimpleMessageBroker.Models
{
    [JsonObject("topic")]
    public class Topic
    {
        [Key]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("subscribers")]
        public List<Subscriber> Subscribers { get; set; }
    }
}
