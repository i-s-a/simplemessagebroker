﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace SimpleMessageBroker.Models
{
    [JsonObject("queue")]
    public class Queue
    {
        [Key]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("messages")]
        public List<PublishedQueueMessage> Messages { get; set; }
    }
}
