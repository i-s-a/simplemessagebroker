﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace SimpleMessageBroker.Models
{
    [JsonObject("subscriber")]
    public class Subscriber
    {
        [Key]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string NotificationUrl { get; set; }
    }
}
