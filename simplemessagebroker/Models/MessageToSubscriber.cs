﻿using System;
using Newtonsoft.Json;

namespace SimpleMessageBroker.Models
{
    [JsonObject("message")]
    public class MessageToSubscriber
    {
        [JsonProperty("timestamp")]
        public DateTime TimeStamp { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
