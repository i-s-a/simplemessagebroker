﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using SimpleMessageBroker.Services;
using SimpleMessageBroker.Models;
using SimpleMessageBroker.Repositories;

namespace SimpleMessageBroker
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase(databaseName: "SimpleMessageBroker"));

            //services.AddScoped<IRepository<Queue>, InMemoryQueueRepository>();
            //services.AddScoped<IRepository<Topic>, InMemoryTopicRepository>();

            services.AddScoped<IQueueRepository, InMemoryQueueRepository>();
            services.AddScoped<ITopicRepository, InMemoryTopicRepository>();

            services.AddScoped(typeof(QueueService));
            services.AddScoped(typeof(TopicService));

            //For now we are creating a single instance of the SubscriberHttpClient that assumes
            //the host is localhost and the port is 12345. Will need to ideally create a separate
            //class (that gets added as a singleton) that can manage a dictionary of
            //SubscriberHttpClient instances keyed on the base uri
            //services.AddSingleton(x => new SubscriberHttpClient("localhost:12345"));

            //Create a single instance the SubscriberHttpClientManager. This will allow us to reuse
            //SubscriberHttpClient instances where they have the same base URI
            services.AddSingleton(typeof(SubscriberHttpClientManager));

            ////I think this needs to be a singleton because we are storing the internal queues
            ////and their subscribers in-memory in the UserService. If we were to store them in
            ////a remote location of some kind (i.e. remote file server, in a database, etc.),
            ////which would be preferable, then we can add the UserService using AddScoped. Currently
            ////because the internal queues are stored in-memory we need to protect them from being
            ////accessed by multiple threads. Ideally we need to move this to some kind of remote
            ////access (e.g. remote file server, database, redis cache) so that we can go back to
            ////having a stateless application server.

            ////services.AddScoped(typeof(QueueService));
            //services.AddSingleton(typeof(QueueService));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Add Swagger generation services to the container
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Pet Game API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Simple Message Broker API v1");
            });

        }
    }
}
