﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Responses
{
    [JsonObject("subscriber")]
    public class TopicSubscriberResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string NotificationUrl { get; set; }
    }
}
