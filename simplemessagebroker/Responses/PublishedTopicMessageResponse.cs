﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Responses
{
    [JsonObject("message")]
    public class PublishedTopicMessageResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("ref")]
        public string Ref { get; set; }
    }
}
