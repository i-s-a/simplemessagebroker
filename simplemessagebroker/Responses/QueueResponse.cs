﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Responses
{
    [JsonObject("queue")]
    public class QueueResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("ref")]
        public string Ref { get; set; }
    }
}
