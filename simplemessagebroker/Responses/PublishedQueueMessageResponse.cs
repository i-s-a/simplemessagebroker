﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Responses
{
    [JsonObject("message")]
    public class PublishedQueueMessageResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("ref")]
        public string Ref { get; set; }
    }
}
