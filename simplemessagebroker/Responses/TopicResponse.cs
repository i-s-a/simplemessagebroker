﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SimpleMessageBroker.Responses
{
    [JsonObject("topic")]
    public class TopicResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("subscribers")]
        public List<TopicSubscriberResponse> Subscribers { get; set; }

        [JsonProperty("ref")]
        public string Ref { get; set; }
    }
}
