﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleMessageBroker.Models;

namespace SimpleMessageBroker
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) 
            : base(options)
        {
        }

        public DbSet<Queue> Queues { get; set; }
        public DbSet<PublishedQueueMessage> PublishedQueueMessages { get; set; }
        public DbSet<Topic> Topics { get; set; }
    }
}
