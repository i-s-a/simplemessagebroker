﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace SimpleMessageBroker
{
    public class SubscriberHttpClient
    {
        private readonly HttpClient mHttpClient = null;
        
        public SubscriberHttpClient(string baseUri)
        {
            Uri uri = new Uri(baseUri);

            mHttpClient = new HttpClient
            {
                BaseAddress = uri
            };

            mHttpClient.DefaultRequestHeaders.Accept.Clear();
            mHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<bool> Post(string subscriberId, string uri, string content)
        {
            try
            {
                HttpResponseMessage httpResponseMessage = await mHttpClient.PostAsync(uri, new StringContent(content));

                httpResponseMessage.EnsureSuccessStatusCode();
            }
            catch(HttpRequestException ex)
            {
                throw new SubscriberSendException(subscriberId, mHttpClient.BaseAddress.ToString(),
                    "Failed to send message: " + uri, ex);
            }

            return true;
        }

        public async Task<bool> PostAsJson<T>(string subscriberId, string uri, T content)
        {
            try
            {
                string json = JsonConvert.SerializeObject(content);

                HttpResponseMessage httpResponseMessage = 
                    await mHttpClient.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

                httpResponseMessage.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException ex)
            {
                throw new SubscriberSendException(subscriberId, mHttpClient.BaseAddress.ToString(),
                    "Failed to send message: " + uri, ex);
            }

            return true;
        }
    }
}
