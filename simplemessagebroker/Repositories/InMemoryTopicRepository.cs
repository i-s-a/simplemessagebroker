﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleMessageBroker.Models;

namespace SimpleMessageBroker.Repositories
{
    public class InMemoryTopicRepository : ITopicRepository
    {
        private ApiContext mContext;

        public InMemoryTopicRepository(ApiContext context)
        {
            mContext = context;
        }

        public async Task AddAsync(Topic item)
        {
            mContext.Topics.Add(item);
            await mContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Topic item)
        {
            mContext.Topics.Remove(item);
            await mContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Topic item)
        {
            mContext.Topics.Update(item);
            await mContext.SaveChangesAsync();
        }

        public async Task<Topic> GetAsync(string id)
        {
            var topic = await mContext.Topics
                .Include(x => x.Subscribers)
                .SingleOrDefaultAsync(x => x.Id == id);

            return topic;
        }

        public async Task<List<Topic>> GetAllAsync()
        {
            var topics = await mContext.Topics
                .Include(x => x.Subscribers)
                .ToListAsync();

            return topics;
        }

        public async Task<List<Topic>> GetAllAsync(int offset, int count)
        {
            var topics = await mContext.Topics
                .Include(x => x.Subscribers)
                .Skip(offset)
                .Take(count)
                .ToListAsync();

            return topics;
        }
    }
}
