﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleMessageBroker.Models;

namespace SimpleMessageBroker.Repositories
{
    public interface IQueueRepository
    {
        Task AddAsync(Queue item);
        Task DeleteAsync(Queue item);
        Task UpdateAsync(Queue item);
        Task<Queue> GetAsync(string id);
        Task<List<Queue>> GetAllAsync();
        Task<List<Queue>> GetAllAsync(int offset, int count);

        Task<PublishedQueueMessage> GetQueueMessage(string queueId, int messageId);
        Task<List<PublishedQueueMessage>> GetQueueMessages(string queueId, int? offset, int? count);
    }
}
