﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleMessageBroker.Models;

namespace SimpleMessageBroker.Repositories
{
    public interface ITopicRepository
    {
        Task AddAsync(Topic item);
        Task DeleteAsync(Topic item);
        Task UpdateAsync(Topic item);
        Task<Topic> GetAsync(string id);
        Task<List<Topic>> GetAllAsync();
        Task<List<Topic>> GetAllAsync(int offset, int count);
    }
}
