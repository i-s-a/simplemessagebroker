﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleMessageBroker.Models;

namespace SimpleMessageBroker.Repositories
{
    public class InMemoryQueueRepository : IQueueRepository
    {
        private ApiContext mContext;

        public InMemoryQueueRepository(ApiContext context)
        {
            mContext = context;
        }

        public async Task AddAsync(Queue queue)
        {
            mContext.Queues.Add(queue);
            await mContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Queue queue)
        {
            mContext.Queues.Remove(queue);
            await mContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Queue queue)
        {
            mContext.Queues.Update(queue);
            await mContext.SaveChangesAsync();
        }

        public async Task<Queue> GetAsync(string id)
        {
            var queue = await mContext.Queues.SingleOrDefaultAsync(x => x.Id == id);
            
            return queue;
        }

        public async Task<List<Queue>> GetAllAsync()
        {
            var queues = await mContext.Queues.ToListAsync();
            return queues;
        }

        public async Task<List<Queue>> GetAllAsync(int offset, int count)
        {
            var queues = await mContext.Queues
                .Skip(offset)
                .Take(count)
                .ToListAsync();

            return queues;
        }

        public async Task<PublishedQueueMessage> GetQueueMessage(string queueId, int messageId)
        {
            var message = await mContext.Queues
                .Where(q => q.Id == queueId)
                .Select(m => m.Messages.SingleOrDefault(y => y.Id == messageId)).SingleOrDefaultAsync();

            return message;
        }

        public async Task<List<PublishedQueueMessage>> GetQueueMessages(string queueId, int? offset, int? count)
        {
            if (offset.HasValue && count.HasValue)
            {
                int skipValue = offset.Value - 1;
                int takeValue = count.Value;

                var messages = await mContext.Queues
                    .Where(q => q.Id == queueId)
                    .Select(m => m.Messages.Skip(skipValue).Take(takeValue).ToList())
                    .SingleOrDefaultAsync();

                return messages;
            }
            else
            {
                var messages = await mContext.Queues
                    .Where(q => q.Id == queueId)
                    .Select(m => m.Messages)
                    .SingleOrDefaultAsync();

                return messages;
            }
        }
    }
}
