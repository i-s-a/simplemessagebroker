﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Requests
{
    [JsonObject("update_queue")]
    public class UpdateQueueRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("num_retries")]
        public int NumRetries { get; set; }
    }
}
