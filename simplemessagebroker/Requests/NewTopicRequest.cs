﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Requests
{
    [JsonObject("new_topic")]
    public class NewTopicRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("num_retries")]
        public int NumRetries { get; set; }
    }
}
