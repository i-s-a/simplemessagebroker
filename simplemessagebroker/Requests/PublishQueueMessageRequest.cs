﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Requests
{
    [JsonObject("publish_message")]
    public class PublishQueueMessageRequest
    {
        [JsonProperty("msg_content")]
        public string MessageContent { get; set; }
    }
}
