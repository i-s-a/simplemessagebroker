﻿using Newtonsoft.Json;

namespace SimpleMessageBroker.Requests
{
    [JsonObject("update_subscriber")]
    public class UpdateTopicSubscriberRequest
    {
        [JsonProperty("new_name")]
        public string NewName { get; set; }

        [JsonProperty("notification_url")]
        public string NotificationUrl { get; set; }
    }
}
