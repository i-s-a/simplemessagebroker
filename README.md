This is an implementation, in .NET Core 2.2, of a simple message broker accessed via a RESTful API.

Publish/Subscribe

The API allows callers to

* Manage multiple topics
* Publish messages to a topic
* Subscribe to topics so that they receive messages published to those topics


Queues

The API allows the caller to

* Manage multiple Queues
* Publish messages to a queue
* Poll one or more queues for all messages starting at a paricular message ID

For simplicity all messages are currently stored in-memory

The project supports documentation generated using Swagger. The message broker can be called using REST calls or using either the QueueClientConnection or TopicClientConnection
