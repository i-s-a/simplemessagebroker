﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

using SimpleMessageBroker;
using SimpleMessageBroker.Requests;
using SimpleMessageBroker.Services;
using SimpleMessageBroker.Models;
using SimpleMessageBroker.Repositories;


namespace SimpleMessageBrokerTest
{
    [TestClass]
    public class QueueServiceTest
    {
        private QueueService mQueueService = null;

        [TestInitialize]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                .UseInMemoryDatabase(databaseName: "TestRepository_QueueService")
                .Options;

            IQueueRepository queueRepository = new InMemoryQueueRepository(new ApiContext(options));
            mQueueService = new QueueService(queueRepository);
        }

        [TestMethod]
        public async Task AddQueue_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_1",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            var queue = await mQueueService.GetQueue(queueId);
            Assert.IsNotNull(queue);
            Assert.IsTrue(queue.Id == queueId);
            Assert.IsTrue(queue.Name == "Queue_1");
        }

        [TestMethod]
        public async Task AddQueue_NoName_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "",
                NumRetries = 0
            }));
        }

        [TestMethod]
        public async Task DeleteQueue_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_2",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            await mQueueService.DeleteQueue(queueId);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mQueueService.GetQueue(queueId));
        }

        [TestMethod]
        public async Task DeleteQueue_NotExist_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_2a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mQueueService.DeleteQueue("random"));
        }

        [TestMethod]
        public async Task UpdateQueue_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_3",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            string updatedQueueId = await mQueueService.UpdateQueue(queueId, new UpdateQueueRequest()
            {
                Name = "Queue_3a_Updated",
                NumRetries = 1
            });

            Assert.IsFalse(string.IsNullOrEmpty(updatedQueueId));

            var queue = await mQueueService.GetQueue(updatedQueueId);
            Assert.IsNotNull(queue);
            Assert.IsTrue(queue.Id == updatedQueueId);
            Assert.IsTrue(queue.Name == "Queue_3a_Updated");
        }

        [TestMethod]
        public async Task UpdateQueue_NoName_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_3b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mQueueService.UpdateQueue(queueId, new UpdateQueueRequest()
            {
                Name = "",
                NumRetries = 1
            }));
        }

        [TestMethod]
        public async Task UpdateQueue_NotExists_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_3c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            var updatedQueueId = await mQueueService.UpdateQueue("Queue_3c_new", new UpdateQueueRequest()
            {
                Name = "Queue_3c_new_name",
                NumRetries = 1
            });

            Assert.IsFalse(string.IsNullOrEmpty(updatedQueueId));

            var updatedQueue = await mQueueService.GetQueue(updatedQueueId);
            Assert.IsNotNull(updatedQueue);
            Assert.IsTrue(updatedQueue.Id == "Queue_3c_new");
            Assert.IsTrue(updatedQueue.Name == "Queue_3c_new_name");
        }

        [TestMethod]
        public async Task GetQueue_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_4",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            var queue = await mQueueService.GetQueue(queueId);
            Assert.IsNotNull(queue);
            Assert.IsTrue(queue.Id == queueId);
            Assert.IsTrue(queue.Name == "Queue_4");
        }

        [TestMethod]
        public async Task GetQueue_NotExist_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_4",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId));

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mQueueService.GetQueue("random"));
        }

        [TestMethod]
        public async Task GetAllQueues_Test()
        {
            string queueId_a = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_5a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_a));

            string queueId_b = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_5b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_b));

            string queueId_c = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_5c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_c));

            var queues = await mQueueService.GetAllQueues(null, null);
            Assert.IsNotNull(queues);
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_a));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_b));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_c));
        }

        [TestMethod]
        public async Task GetAllQueues_Count_And_Offset_Test()
        {
            string queueId_a = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_6a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_a));

            string queueId_b = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_6b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_b));

            string queueId_c = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_6c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_c));

            string queueId_d = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_6d",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_d));

            var queues = await mQueueService.GetAllQueues(0, 2);
            Assert.IsNotNull(queues);
            Assert.IsTrue(queues.Count == 2);

            queues = await mQueueService.GetAllQueues(1, 2);
            Assert.IsNotNull(queues);
            Assert.IsTrue(queues.Count == 2);

            queues = await mQueueService.GetAllQueues(2, 2);
            Assert.IsNotNull(queues);
            Assert.IsTrue(queues.Count == 2);

            queues = await mQueueService.GetAllQueues(3, 2);
            Assert.IsNotNull(queues);
            Assert.IsTrue(queues.Count == 2);
        }

        [TestMethod]
        public async Task GetAllQueues_Count_No_Offset()
        {
            string queueId_a = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_7a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_a));

            string queueId_b = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_7b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_b));

            string queueId_c = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_7c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_c));

            string queueId_d = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_7d",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_d));

            var queues = await mQueueService.GetAllQueues(0, null);
            Assert.IsNotNull(queues);
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_a));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_b));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_c));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_d));
        }

        [TestMethod]
        public async Task GetAllQueues_Offset_No_Count()
        {
            string queueId_a = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_8a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_a));

            string queueId_b = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_8b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_b));

            string queueId_c = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_8c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_c));

            string queueId_d = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_8d",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(queueId_d));

            var queues = await mQueueService.GetAllQueues(null, 2);
            Assert.IsNotNull(queues);
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_a));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_b));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_c));
            Assert.IsTrue(queues.Exists(x => x.Id == queueId_d));
        }

        [TestMethod]
        public async Task PublishMessageToQueue_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_9",
                NumRetries = 0
            });

            int offset = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "This is some message content"
            });

            var message = await mQueueService.GetQueueMessage(queueId, offset);
            Assert.IsNotNull(message);
            Assert.IsTrue(message.Id == offset);
            Assert.IsTrue(message.Content == "This is some message content");

            //var messages = await mQueueService.GetQueueMessages(queueId, offset, 1);
            //Assert.IsNotNull(messages);
            //Assert.IsTrue(messages[0].Id == offset);
            //Assert.IsTrue(messages[0].Content == "This is some message content");

            //var messages = await mQueueService.GetQueueMessages(queueId, null, null);
            //Assert.IsNotNull(messages);
            //Assert.IsTrue(messages[0].Id == offset);
            //Assert.IsTrue(messages[0].Content == "This is some message content");
        }

        [TestMethod]
        public async Task PublishMessageToQueue_QueueNotExist_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_9a",
                NumRetries = 0
            });

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mQueueService.PublishMessageToQueue("random", new PublishQueueMessageRequest()
            {
                MessageContent = "This is some message content"
            }));
        }

        [TestMethod]
        public async Task GetQueueMessage_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_10",
                NumRetries = 0
            });

            int offset_1 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 1"
            });

            int offset_2 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 2"
            });

            var message_1 = await mQueueService.GetQueueMessage(queueId, offset_1);
            Assert.IsNotNull(message_1);
            Assert.IsTrue(message_1.Id == offset_1);
            Assert.IsTrue(message_1.Content == "Message content 1");

            var message_2 = await mQueueService.GetQueueMessage(queueId, offset_2);
            Assert.IsNotNull(message_2);
            Assert.IsTrue(message_2.Id == offset_2);
            Assert.IsTrue(message_2.Content == "Message content 2");
        }

        [TestMethod]
        public async Task GetQueueMessage_QueueNotExist_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_10a",
                NumRetries = 0
            });

            int offset_1 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 1"
            });

            int offset_2 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 2"
            });

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mQueueService.GetQueueMessage("random", offset_1));
        }

        [TestMethod]
        public async Task GetQueueMessage_MessageNotExist_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_10b",
                NumRetries = 0
            });

            int offset_1 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 1"
            });

            int offset_2 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 2"
            });

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mQueueService.GetQueueMessage(queueId, 1_000_000));
        }

        [TestMethod]
        public async Task GetAllQueueMessages_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_11",
                NumRetries = 0
            });

            int offset_1 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 1"
            });

            int offset_2 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 2"
            });

            int offset_3 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 3"
            });

            int offset_4 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 4"
            });

            int offset_5 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 5"
            });

            int offset_6 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 6"
            });

            var messages = await mQueueService.GetQueueMessages(queueId, null, null);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_1);
            Assert.IsTrue(messages[0].Content == "Message content 1");
            Assert.IsTrue(messages[1].Id == offset_2);
            Assert.IsTrue(messages[1].Content == "Message content 2");
            Assert.IsTrue(messages[2].Id == offset_3);
            Assert.IsTrue(messages[2].Content == "Message content 3");
            Assert.IsTrue(messages[3].Id == offset_4);
            Assert.IsTrue(messages[3].Content == "Message content 4");
            Assert.IsTrue(messages[4].Id == offset_5);
            Assert.IsTrue(messages[4].Content == "Message content 5");
            Assert.IsTrue(messages[5].Id == offset_6);
            Assert.IsTrue(messages[5].Content == "Message content 6");
        }

        [TestMethod]
        public async Task GetAllQueueMessages_CountNoOffset_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_11a",
                NumRetries = 0
            });

            int offset_1 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 1"
            });

            int offset_2 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 2"
            });

            int offset_3 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 3"
            });

            int offset_4 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 4"
            });

            int offset_5 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 5"
            });

            int offset_6 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 6"
            });

            var messages = await mQueueService.GetQueueMessages(queueId, null, 3);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_1);
            Assert.IsTrue(messages[0].Content == "Message content 1");
            Assert.IsTrue(messages[1].Id == offset_2);
            Assert.IsTrue(messages[1].Content == "Message content 2");
            Assert.IsTrue(messages[2].Id == offset_3);
            Assert.IsTrue(messages[2].Content == "Message content 3");
            Assert.IsTrue(messages[3].Id == offset_4);
            Assert.IsTrue(messages[3].Content == "Message content 4");
            Assert.IsTrue(messages[4].Id == offset_5);
            Assert.IsTrue(messages[4].Content == "Message content 5");
            Assert.IsTrue(messages[5].Id == offset_6);
            Assert.IsTrue(messages[5].Content == "Message content 6");
        }

        [TestMethod]
        public async Task GetAllQueueMessages_OffsetNoCount_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_11b",
                NumRetries = 0
            });

            int offset_1 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 1"
            });

            int offset_2 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 2"
            });

            int offset_3 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 3"
            });

            int offset_4 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 4"
            });

            int offset_5 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 5"
            });

            int offset_6 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 6"
            });

            var messages = await mQueueService.GetQueueMessages(queueId, null, 3);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_1);
            Assert.IsTrue(messages[0].Content == "Message content 1");
            Assert.IsTrue(messages[1].Id == offset_2);
            Assert.IsTrue(messages[1].Content == "Message content 2");
            Assert.IsTrue(messages[2].Id == offset_3);
            Assert.IsTrue(messages[2].Content == "Message content 3");
            Assert.IsTrue(messages[3].Id == offset_4);
            Assert.IsTrue(messages[3].Content == "Message content 4");
            Assert.IsTrue(messages[4].Id == offset_5);
            Assert.IsTrue(messages[4].Content == "Message content 5");
            Assert.IsTrue(messages[5].Id == offset_6);
            Assert.IsTrue(messages[5].Content == "Message content 6");
        }

        [TestMethod]
        public async Task GetAllQueueMessages_Paged_Test()
        {
            string queueId = await mQueueService.AddQueue(new NewQueueRequest()
            {
                Name = "Queue_11c",
                NumRetries = 0
            });

            int offset_1 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 1"
            });

            int offset_2 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 2"
            });

            int offset_3 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 3"
            });

            int offset_4 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 4"
            });

            int offset_5 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 5"
            });

            int offset_6 = await mQueueService.PublishMessageToQueue(queueId, new PublishQueueMessageRequest()
            {
                MessageContent = "Message content 6"
            });

            var messages = await mQueueService.GetQueueMessages(queueId, 1, 4);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_1);
            Assert.IsTrue(messages[0].Content == "Message content 1");
            Assert.IsTrue(messages[1].Id == offset_2);
            Assert.IsTrue(messages[1].Content == "Message content 2");
            Assert.IsTrue(messages[2].Id == offset_3);
            Assert.IsTrue(messages[2].Content == "Message content 3");
            Assert.IsTrue(messages[3].Id == offset_4);
            Assert.IsTrue(messages[3].Content == "Message content 4");

            messages = await mQueueService.GetQueueMessages(queueId, 2, 5);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_2);
            Assert.IsTrue(messages[0].Content == "Message content 2");
            Assert.IsTrue(messages[1].Id == offset_3);
            Assert.IsTrue(messages[1].Content == "Message content 3");
            Assert.IsTrue(messages[2].Id == offset_4);
            Assert.IsTrue(messages[2].Content == "Message content 4");
            Assert.IsTrue(messages[3].Id == offset_5);
            Assert.IsTrue(messages[3].Content == "Message content 5");

            messages = await mQueueService.GetQueueMessages(queueId, 3, 6);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_3);
            Assert.IsTrue(messages[0].Content == "Message content 3");
            Assert.IsTrue(messages[1].Id == offset_4);
            Assert.IsTrue(messages[1].Content == "Message content 4");
            Assert.IsTrue(messages[2].Id == offset_5);
            Assert.IsTrue(messages[2].Content == "Message content 5");
            Assert.IsTrue(messages[3].Id == offset_6);
            Assert.IsTrue(messages[3].Content == "Message content 6");

            messages = await mQueueService.GetQueueMessages(queueId, 4, 7);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_4);
            Assert.IsTrue(messages[0].Content == "Message content 4");
            Assert.IsTrue(messages[1].Id == offset_5);
            Assert.IsTrue(messages[1].Content == "Message content 5");
            Assert.IsTrue(messages[2].Id == offset_6);
            Assert.IsTrue(messages[2].Content == "Message content 6");

            messages = await mQueueService.GetQueueMessages(queueId, 5, 8);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_5);
            Assert.IsTrue(messages[0].Content == "Message content 5");
            Assert.IsTrue(messages[1].Id == offset_6);
            Assert.IsTrue(messages[1].Content == "Message content 6");

            messages = await mQueueService.GetQueueMessages(queueId, 6, 9);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages[0].Id == offset_6);
            Assert.IsTrue(messages[0].Content == "Message content 6");

            messages = await mQueueService.GetQueueMessages(queueId, 7, 10);
            Assert.IsNotNull(messages);
            Assert.IsTrue(messages.Count == 0);
        }
    }
}
