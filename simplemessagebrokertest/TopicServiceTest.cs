using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

using SimpleMessageBroker;
using SimpleMessageBroker.Requests;
using SimpleMessageBroker.Services;
using SimpleMessageBroker.Models;
using SimpleMessageBroker.Repositories;

namespace SimpleMessageBrokerTest
{
    [TestClass]
    public class TopicServiceTest
    {
        private TopicService mTopicService = null;

        [TestInitialize]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                .UseInMemoryDatabase(databaseName: "TestRepository_TopicService")
                .Options;

            ITopicRepository topicRepository = new InMemoryTopicRepository(new ApiContext(options));
            SubscriberHttpClientManager subscriberHttpClientManager = new SubscriberHttpClientManager();

            mTopicService = new TopicService(topicRepository, subscriberHttpClientManager);
        }

        [TestMethod]
        public async Task AddTopic_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_1",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var topic = await mTopicService.GetTopic(topicId);
            Assert.IsNotNull(topic);
            Assert.IsTrue(topic.Id == topicId);
            Assert.IsTrue(topic.Name == "Topic_1");
        }

        [TestMethod]
        public async Task AddTopic_NoName_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "",
                NumRetries = 0
            }));
        }

        [TestMethod]
        public async Task DeleteTopic_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_2",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await mTopicService.DeleteTopic(topicId);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.GetTopic(topicId));
        }

        [TestMethod]
        public async Task DeleteTopic_NotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_2a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.DeleteTopic("random"));
        }

        [TestMethod]
        public async Task UpdateTopic_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_3",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await mTopicService.UpdateTopic(topicId, new UpdateTopicRequest()
            {
                Name = "Topic_3a_Updated",
                NumRetries = 1
            });

            var topic = await mTopicService.GetTopic(topicId);
            Assert.IsNotNull(topic);
            Assert.IsTrue(topic.Id == topicId);
            Assert.IsTrue(topic.Name == "Topic_3a_Updated");
        }

        [TestMethod]
        public async Task UpdateTopic_NoName_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_3b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.UpdateTopic(topicId, new UpdateTopicRequest()
            {
                Name = "",
                NumRetries = 1
            }));
        }

        [TestMethod]
        public async Task UpdateTopic_TopicNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_3c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var updatedTopicId = await mTopicService.UpdateTopic("Topic_3c_new", new UpdateTopicRequest()
            {
                Name = "Topic_3c_new_name",
                NumRetries = 1
            });

            var topic = await mTopicService.GetTopic(updatedTopicId);
            Assert.IsNotNull(topic);
            Assert.IsTrue(topic.Id == "Topic_3c_new");
            Assert.IsTrue(topic.Name == "Topic_3c_new_name");
        }

        [TestMethod]
        public async Task GetAllTopics_Test()
        {
            string topicId_1 = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_4",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId_1));

            string topicId_2 = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_4a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId_2));

            var topics = await mTopicService.GetAllTopics();
            Assert.IsTrue(topics.Exists(x => x.Id == topicId_1));
            Assert.IsTrue(topics.Exists(x => x.Id == topicId_2));
        }

        [TestMethod]
        public async Task GetTopic_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_4b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var topic = await mTopicService.GetTopic(topicId);
            Assert.IsNotNull(topic);
            Assert.IsTrue(topic.Id == topicId);
            Assert.IsTrue(topic.Name == "Topic_4b");
        }

        [TestMethod]
        public async Task GetTopic_NotExist_Test()
        {
            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.GetTopic("random"));
        }

        [TestMethod]
        public async Task AddSubscriber_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_5",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");
        }

        [TestMethod]
        public async Task AddSubscriber_TopicNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_5a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.AddSubscriber("random", new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            }));
        }

        [TestMethod]
        public async Task AddSubscriber_NoName_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_5b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "",
                NotificationUrl = "http://localhost:11111/messages"
            }));
        }

        [TestMethod]
        public async Task AddSubscriber_InvalidUrl_Empty_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_5c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = ""
            }));

            //await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await subscriberId);
        }

        [TestMethod]
        public async Task AddSubscriber_InvalidUrl_Protocol_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_5c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "htp://localhost:11111/messages"
            }));
        }

        [TestMethod]
        public async Task AddSubscriber_InvalidUrl_UrlFormat_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_5c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http:/localhost:11111/messages"
            }));
        }

        [TestMethod]
        public async Task UpdateSubscriber_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_6",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await mTopicService.UpdateSubscriber(topicId, subscriberId, new UpdateTopicSubscriberRequest()
            {
                NewName = "Han Solo",
                NotificationUrl = "http://localhost:11111/received_messages"
            });

            subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Han Solo");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/received_messages");
        }

        [TestMethod]
        public async Task UpdateSubscriber_TopicNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_6a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.UpdateSubscriber("random", subscriberId, new UpdateTopicSubscriberRequest()
            {
                NewName = "Han Solo",
                NotificationUrl = "http://localhost:11111/received_messages"
            }));
        }

        [TestMethod]
        public async Task UpdateSubscriber_NoName_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_6b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.UpdateSubscriber(topicId, subscriberId, new UpdateTopicSubscriberRequest()
            {
                NewName = "",
                NotificationUrl = "http://localhost:11111/received_messages"
            }));
        }

        [TestMethod]
        public async Task UpdateSubscriber_InvalidUrl_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_6c",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mTopicService.UpdateSubscriber(topicId, subscriberId, new UpdateTopicSubscriberRequest()
            {
                NewName = "Han Solo",
                NotificationUrl = ""
            }));
        }

        [TestMethod]
        public async Task UpdateSubscriber_SubscriberNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_6d",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.UpdateSubscriber(topicId, "random", new UpdateTopicSubscriberRequest()
            {
                NewName = "Han Solo",
                NotificationUrl = "http://localhost:11111/received_messages"
            }));
        }

        [TestMethod]
        public async Task DeleteSubscriber_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_7",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await mTopicService.DeleteSubscriber(topicId, subscriberId);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.GetSubscriber(topicId, subscriberId));
        }

        [TestMethod]
        public async Task DeleteSubscriber_TopicNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_7a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.DeleteSubscriber("random", subscriberId));
        }

        [TestMethod]
        public async Task DeleteSubscriber_SubscriberNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_7b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.DeleteSubscriber(topicId, "random"));
        }

        [TestMethod]
        public async Task GetSubscriber_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_8",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            var subscriber = await mTopicService.GetSubscriber(topicId, subscriberId);
            Assert.IsNotNull(subscriber);
            Assert.IsTrue(subscriber.Id == subscriberId);
            Assert.IsTrue(subscriber.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber.NotificationUrl == "http://localhost:11111/messages");
        }

        [TestMethod]
        public async Task GetSubscriber_TopicNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_8a",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.GetSubscriber("random", subscriberId));
        }

        [TestMethod]
        public async Task GetSubscriber_SubscriberNotExist_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_8b",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages"
            });

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mTopicService.GetSubscriber(topicId, "random"));
        }

        [TestMethod]
        public async Task GetAllSubscribers_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_9",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId_1 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages_1"
            });

            var subscriberId_2 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Han Solo",
                NotificationUrl = "http://localhost:11111/messages_2"
            });

            var subscriberId_3 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "R2D2",
                NotificationUrl = "http://127.0.0.1:11111/messages_3"
            });

            var subscriberId_4 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "C3P0",
                NotificationUrl = "http://127.0.0.1:11111/messages_4"
            });

            var topic = await mTopicService.GetTopic(topicId);
            var topicSubscribers = topic.Subscribers;

            var subscriber_1 = topicSubscribers.SingleOrDefault(x => x.Id == subscriberId_1);
            Assert.IsNotNull(subscriber_1);
            Assert.IsTrue(subscriber_1.Id == subscriberId_1);
            Assert.IsTrue(subscriber_1.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber_1.NotificationUrl == "http://localhost:11111/messages_1");

            var subscriber_2 = topicSubscribers.SingleOrDefault(x => x.Id == subscriberId_2);
            Assert.IsNotNull(subscriber_2);
            Assert.IsTrue(subscriber_2.Id == subscriberId_2);
            Assert.IsTrue(subscriber_2.Name == "Han Solo");
            Assert.IsTrue(subscriber_2.NotificationUrl == "http://localhost:11111/messages_2");

            var subscriber_3 = topicSubscribers.SingleOrDefault(x => x.Id == subscriberId_3);
            Assert.IsNotNull(subscriber_3);
            Assert.IsTrue(subscriber_3.Id == subscriberId_3);
            Assert.IsTrue(subscriber_3.Name == "R2D2");
            Assert.IsTrue(subscriber_3.NotificationUrl == "http://127.0.0.1:11111/messages_3");

            var subscriber_4 = topicSubscribers.SingleOrDefault(x => x.Id == subscriberId_4);
            Assert.IsNotNull(subscriber_4);
            Assert.IsTrue(subscriber_4.Id == subscriberId_4);
            Assert.IsTrue(subscriber_4.Name == "C3P0");
            Assert.IsTrue(subscriber_4.NotificationUrl == "http://127.0.0.1:11111/messages_4");


            var subscribers = await mTopicService.GetSubsribers(topicId);

            subscriber_1 = subscribers.SingleOrDefault(x => x.Id == subscriberId_1);
            Assert.IsNotNull(subscriber_1);
            Assert.IsTrue(subscriber_1.Id == subscriberId_1);
            Assert.IsTrue(subscriber_1.Name == "Luke Skywalker");
            Assert.IsTrue(subscriber_1.NotificationUrl == "http://localhost:11111/messages_1");

            subscriber_2 = subscribers.SingleOrDefault(x => x.Id == subscriberId_2);
            Assert.IsNotNull(subscriber_2);
            Assert.IsTrue(subscriber_2.Id == subscriberId_2);
            Assert.IsTrue(subscriber_2.Name == "Han Solo");
            Assert.IsTrue(subscriber_2.NotificationUrl == "http://localhost:11111/messages_2");

            subscriber_3 = subscribers.SingleOrDefault(x => x.Id == subscriberId_3);
            Assert.IsNotNull(subscriber_3);
            Assert.IsTrue(subscriber_3.Id == subscriberId_3);
            Assert.IsTrue(subscriber_3.Name == "R2D2");
            Assert.IsTrue(subscriber_3.NotificationUrl == "http://127.0.0.1:11111/messages_3");

            subscriber_4 = subscribers.SingleOrDefault(x => x.Id == subscriberId_4);
            Assert.IsNotNull(subscriber_4);
            Assert.IsTrue(subscriber_4.Id == subscriberId_4);
            Assert.IsTrue(subscriber_4.Name == "C3P0");
            Assert.IsTrue(subscriber_4.NotificationUrl == "http://127.0.0.1:11111/messages_4");
        }

        [TestMethod]
        public async Task PublishMessageToTopic_InvalidUrls_Test()
        {
            string topicId = await mTopicService.AddTopic(new NewTopicRequest()
            {
                Name = "Topic_10",
                NumRetries = 0
            });

            Assert.IsFalse(string.IsNullOrEmpty(topicId));

            var subscriberId_1 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Luke Skywalker",
                NotificationUrl = "http://localhost:11111/messages_1"
            });

            var subscriberId_2 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "Han Solo",
                NotificationUrl = "http://localhost:11111/messages_2"
            });

            var subscriberId_3 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "R2D2",
                NotificationUrl = "http://127.0.0.1:11111/messages_3"
            });

            var subscriberId_4 = await mTopicService.AddSubscriber(topicId, new NewTopicSubscriberRequest()
            {
                Name = "C3P0",
                NotificationUrl = "http://127.0.0.1:11111/messages_4"
            });

            await mTopicService.PublishMessageToTopic(topicId, new PublishTopicMessageRequest()
            {
                MessageContent = "This should really be JSON"
            });

            var subscribers = await mTopicService.GetSubsribers(topicId);
            Assert.IsTrue(subscribers.Count == 0);
        }
    }
}
